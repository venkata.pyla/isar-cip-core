#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.19.y-cip"

SRC_URI[sha256sum] = "436c7578711e67332c7a569fd5966c69e41d872a89faab211fd6b00fe2077b5b"
