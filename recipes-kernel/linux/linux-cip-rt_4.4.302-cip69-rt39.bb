#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2022
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.4.y-cip"

SRC_URI[sha256sum] = "b5f267fa436e5d5e729f1febb741171cb1df5a576400d06999202f733d8e67ed"
