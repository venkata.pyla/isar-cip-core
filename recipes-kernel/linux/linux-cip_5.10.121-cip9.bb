#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2021-2022
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "5.10.y-cip"

SRC_URI[sha256sum] = "0be35d737212ae68cf8dea7c84bfac61f2b98aeec3df6f308692fbdfc80db743"
